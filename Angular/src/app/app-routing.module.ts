import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ProductsComponent } from './products/products.component';
import { AuthGuard } from './auth.guard';
import { ShowusersComponent } from './showusers/showusers.component';
import { LogoutComponent } from './logout/logout.component';
import { CartComponent } from './cart/cart.component';
import { AdminComponent } from './admin/admin.component';
import { AdminActionsComponent } from './admin-actions/admin-actions.component';
import { RegisterProductComponent } from './register-product/register-product.component';
import { HomeComponent } from './home/home.component';


const routes: Routes = [
  {path:"", component:HomeComponent},
  {path:"home", component:HomeComponent},
  {path:"login", component:LoginComponent},
  
  {path:"register", component:RegisterComponent},
  {path:"products", component:ProductsComponent},
  {path:"showusers", component:ShowusersComponent},
  {path:"logout",    canActivate:[AuthGuard], component:LogoutComponent},
  {path:"cart",      canActivate:[AuthGuard], component:CartComponent},
  {path:"admin", component:AdminComponent},
  {path:"admin-actions",component:AdminActionsComponent},
  {path:"register-product",component:RegisterProductComponent}
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
