import { Injectable } from '@angular/core';

import {UserService} from './user.service';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class AuthGuard  {

  constructor(private service: UserService, private router: Router) {}
  canActivate(): boolean {
    // Check if the user is logged in
    if (this.service.getUserLoggedStatus()) {
      return true;
    } else {
      // If the user is not logged in, redirect to the login page
      this.router.navigate(['/login']);
      return false;
    }
  }

  
}
