import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

declare var Razorpay: any;
@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  private cartItems: any[] = [];
  receivedProducts: any;
  products: any;
  user:any;
  name: string = '';
  email: string = '';
  mobileNumber: string = '';
  amount: any;
  totalAmount: number = 0;
  constructor(private service: UserService, private toastr: ToastrService, private router: Router) {
    this.receivedProducts = localStorage.getItem('cartProducts');
    this.products = JSON.parse(this.receivedProducts);
    console.log(this.products);
    this.products = this.service.cartItems;
  }

  ngOnInit() {
    this.getCartProducts();
    this.calculateTotalAmount();
  }
  getCartProducts() {
    const cartProducts = localStorage.getItem('cartProducts');
    if (cartProducts) {
      this.products = JSON.parse(cartProducts);
    }
  }
  placeOrder() {
    this.products = [];
    localStorage.removeItem('cartProducts');
    // this.toastr.success('Order placed successfully!', 'Success');
    if (!this.name || !this.email || !this.mobileNumber ) {
      this.toastr.error('Please enter your name, email, mobile number.', 'Error');
      return;
    }
    const RazorpayOptions = {
      description: 'Sample Razorpay demo',
      currency: 'INR',
      amount: this.totalAmount * 100,
      name: 'bigbasket',
      key: 'rzp_test_kMMQKxEclISDOZ',
      image: '/assets/images/cart.jpg',
      prefill: {
        name: this.name,
        email: this.email,
        phone: this.mobileNumber 
      },
      theme: {
        color: '#d6b9b0'
      },
      modal: {
        ondismiss: () => {
          console.log('Payment dismissed');
        }
      }
    }
    
    const successCallback = (paymentid: any) =>{
      this.router.navigate(['/home']);
    }
    Razorpay.open(RazorpayOptions);
  }
  getCartItems(): any[] {
    return this.cartItems;
  }

  calculateTotalAmount() {
    this.totalAmount = this.products.reduce((total:number, product:any) => total + product.prodPrice, 0);
  }
}


