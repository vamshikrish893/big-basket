import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {
  
  constructor(private userService: UserService, private router: Router) {}

  ngOnInit() {
    if (this.userService.getUserLoggedStatus()) {
      // User is logged in, perform logout
      this.userService.setUserLogout();
    }
    // Redirect to the login page regardless of login status
    this.router.navigate(['login']);
  }
}
