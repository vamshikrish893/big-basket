import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  products: any;
  users: any;
  cartProducts: any;

  constructor(private service: UserService, private router: Router) {
    this.cartProducts = [];
  }

  ngOnInit() {

    this.service.getAllProducts().subscribe((data: any) => {
      this.products = data;
    });
  }

  addToCart(product: any) {
    if (this.service.getUserLoggedStatus()) {
      this.service.addToCart(product);
    } else {
      this.router.navigate(['/login']);
    }
  }


}
