import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-register-product',
  templateUrl: './register-product.component.html',
  styleUrls: ['./register-product.component.css']
})
export class RegisterProductComponent implements OnInit {

  
  product: any;
  products: any[] = [];

  constructor(private service: UserService, private router: Router,private toastr: ToastrService) { 

    this.product = { prodName: '', prodImg: '', proDesc: '', prodPrice: '' }
  }

  ngOnInit(): void {
  }

  registerProduct(regForm: any) {

    this.product.prodName = regForm.prodName;
    this.product.prodImg = regForm.prodImg;
    this.product.proDesc = regForm.proDesc;
    this.product.prodPrice = regForm.prodPrice;

    this.service.registerProduct(this.product).subscribe((result: any) => {
      console.log(result);
      this.products.push(result);
    });
    
  }

  onRegister() {
    this.toastr.success('Product added successfully!', 'Success');
  }
}
