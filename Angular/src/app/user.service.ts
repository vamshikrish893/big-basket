import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  isUserLogged: boolean;
  loginStatus: Subject<any>;
  cartItems: any;
  productToBeAdded: Subject<any>;
  user: any;
  isAdminLogged: boolean;
  adminLoginStatus: Subject<any>
  cartStatus: Subject<any>;

  //Implementing Dependency Injection for HttpClient using Constructor
  constructor(private http: HttpClient) {
    this.isUserLogged = false;
    this.loginStatus = new Subject();
    this.cartItems = [];
    this.productToBeAdded = new Subject();
    this.isAdminLogged = false;
    this.adminLoginStatus = new Subject();
    this.cartStatus = new Subject();
  }


  //LoginSuccess
  setUserLoggedIn() {
    this.isUserLogged = true;
    this.loginStatus.next(true);
  }

  getUserLoggedStatus(): boolean {
    return this.isUserLogged;
  }

  setUserLogout() {
    this.isUserLogged = false;
    this.loginStatus.next(false);
  }


  getAllUsers(): any {
    return this.http.get('http://localhost:8085/getAllUsers');
  }


  getUser(loginForm: any): any {
    return this.http.get('http://localhost:8085/login/' + loginForm.emailId + "/" + loginForm.password).toPromise();
  }


  deleteUser(userId: any): any {
    return this.http.delete('/deleteUser/' + userId)
  }

  registerUser(user: any) {
    return this.http.post('http://localhost:8085/registerUser', user);
  }

  updateUser(user: any): any {
    return this.http.put('/updateUser', user);
  }


  getLoginStatus(): any {
    return this.loginStatus.asObservable();
  }

  addToCart(product: any) {
    this.cartItems.push(product);
    this.productToBeAdded.next(product);
  }

  getProductStatus(): any {
    return this.productToBeAdded.asObservable();
  }

  getAllProducts(): any {
    return this.http.get('http://localhost:8085/getAllProducts');
  }

  registerProduct(product: any): any {
    return this.http.post('http://localhost:8085/registerProduct', product);
  }



}



