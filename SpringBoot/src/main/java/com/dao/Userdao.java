package com.dao;

import java.util.List;
import com.model.User;
import com.ts.EmailService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class Userdao {
	
	@Autowired
	UserRepository userRepo;

	public List<User> getAllUsers() {
		return userRepo.findAll();
	}

	public User getUserById(int id) {
		return userRepo.findById(id).orElse(null);
	}

	public User getUserByName(String name) {
		return userRepo.findByName(name);
	}

	public User registerUser(User user) {
		
		String emailContent = "Welcome to Big Basket - Your Online Grocery Store!\n\n "+"Dear " + user.getUserName() + ",\n\n"
				+ "Welcome to Big Basket! We're excited to have you as a member of our online grocery store. Our team is dedicated to providing you with a convenient and hassle-free shopping experience right from the comfort of your home.\n\n"
				+ "If you have any questions, concerns, or need assistance, our customer support team is always ready to help. Feel free to reach out to us via email or phone, and we'll be happy to assist you.Thank you once again for choosing Big Basket as your trusted online grocery store. We look forward to serving you and providing you with a delightful shopping experience.\n\n" + "Happy shopping!\n\n" + "Big Basket Customer Support Team";
		EmailService.sendEmail(emailContent, "Thank you for choosing Big Basket ", user.getEmailId(),
				"bigbasket691@gmail.com");
		
		String password = user.getPassword();
		System.out.println(password);

		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

		String hashedPassword = passwordEncoder.encode(password);
		System.out.println(hashedPassword);

		user.setPassword(hashedPassword);
		return userRepo.save(user);
	}
	
	public void deleteUser(int userId) {
		userRepo.deleteById(userId);
	}
	
	public User login(String emailId, String password) {
		User user = userRepo.findByEmailId(emailId);

		if (user == null) {
			return null;
		}

		String hashedPassword = user.getPassword();
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

		if (passwordEncoder.matches(password, hashedPassword)) {
			return user;
		} else {
			return null;
		}
	}

}
