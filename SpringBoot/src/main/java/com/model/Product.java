package com.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Product {
 
	@Id@GeneratedValue
	private int prodId;
	@Column(name="productName", length=100)
	private String prodName;
	@Column(name="productImage", length=5000)
	private String prodImg;
	@Column(name="productDescription", length=1000)
	private String proDesc;
	@Column(name="productPrice", length=20)
	private double prodPrice;
	
	public Product() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Product(int prodId, String prodName, String prodImg, String proDesc, double prodPrice) {
		super();
		this.prodId = prodId;
		this.prodName = prodName;
		this.prodImg = prodImg;
		this.proDesc = proDesc;
		this.prodPrice = prodPrice;
	}

	public int getProdId() {
		return prodId;
	}

	public void setProdId(int prodId) {
		this.prodId = prodId;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public String getProdImg() {
		return prodImg;
	}

	public void setProdImg(String prodImg) {
		this.prodImg = prodImg;
	}

	public String getProDesc() {
		return proDesc;
	}

	public void setProDesc(String proDesc) {
		this.proDesc = proDesc;
	}

	public double getProdPrice() {
		return prodPrice;
	}

	public void setProdPrice(double prodPrice) {
		this.prodPrice = prodPrice;
	}

	@Override
	public String toString() {
		return "Product [prodId=" + prodId + ", prodName=" + prodName + ", prodImg=" + prodImg + ", proDesc=" + proDesc
				+ ", prodPrice=" + prodPrice + "]";
	}
	
	
}
