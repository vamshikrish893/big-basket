package com.ts;


import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.ProductDao;
import com.model.Product;
@RestController
public class ProductController {



	//Dependency Injection
	@Autowired
	ProductDao prodDAO;

	@GetMapping("/getAllProducts")
	public List<Product> getAllProducts() {
		return prodDAO.getAllProducts();
	}

	@PostMapping("registerProduct")
	public Product registerProduct(@RequestBody Product product) {	
		return prodDAO.registerProduct(product);
	}

}