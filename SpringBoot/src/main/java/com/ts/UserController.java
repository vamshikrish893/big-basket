package com.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.Userdao;
import com.model.User;

@RestController
public class UserController {
	
	@Autowired
	Userdao userdao;
	
	@GetMapping("getAllUsers")
	public List<User> getAllUsers() {
		return userdao.getAllUsers();
	}
	
	@GetMapping("getUserById/{userId}")
	public User getUserById(@PathVariable("userId") int id) {
		return userdao.getUserById(id);
	}
	
	@GetMapping("getUserByName/{userName}")
	public User getUserByName(@PathVariable("userName") String name) {
		return userdao.getUserByName(name);
	}
	
	@PostMapping("registerUser")
	public User registerUser(@RequestBody User user) {
		return userdao.registerUser(user);
	}
	
	@PutMapping("registerUser")
	public User updateUser(@RequestBody User user) {
	    return userdao.registerUser(user);
	}
	
	@DeleteMapping("deleteUser/{userId}")
	public String deleteUser(@PathVariable int userId) {
		userdao.deleteUser(userId);
	    return "User Deleted Successfully";
	}
	
	@GetMapping("/login/{emailId}/{password}")
	public User login(@PathVariable("emailId") String emailId, @PathVariable("password") String password) {
	    return userdao.login(emailId, password);
	}


}
